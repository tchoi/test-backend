<?php
include_once __DIR__ . "/vendor/autoload.php";

use App\Helpers\Ordenador;
use App\Services\ListaCompras;


$aListaDeCompras = include_once __DIR__ . '/lista-de-compras.php';
$ordenador = new Ordenador($aListaDeCompras);
$listaOrdenada = $ordenador->ordenar();
$listComprasSave = new ListaCompras();

foreach ($listaOrdenada as $item) {
    $listComprasSave->save($item);
}