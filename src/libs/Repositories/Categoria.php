<?php

namespace App\Repositories;


class Categoria extends BaseRepository
{
    public function save($nome)
    {
        $id = $this->verify($nome);
        if (!$id) {
            return $this->insert($nome);
        }
        return $this->update($id, $nome);
    }

    public function verify($nome)
    {
        $sql = "SELECT id FROM categoria where nome =:nome";
        $stmt = $this->pdo->prepare($sql);
        $stmt->bindParam(":nome",$nome);
        if (!$stmt->execute()) {
            return false;
        }
        $reg = $stmt->fetchObject();
        return $reg->id ?? false;
    }

    public function insert($nome)
    {
        $sql = "INSERT INTO categoria SET nome = :nome";
        $stmt = $this->pdo->prepare($sql);
        $stmt->bindParam(":nome",$nome);
        return $stmt->execute() ? $this->pdo->lastInsertId() : false;
    }

    public function update($id, $nome)
    {
        $sql = "UPDATE categoria SET nome = :nome WHERE id = :id";
        $stmt = $this->pdo->prepare($sql);
        $stmt->bindParam(":nome",$nome);
        $stmt->bindParam(":id",$id);
        return $stmt->execute() ? $id : false;
    }
}