<?php

namespace App\Repositories;


class Produto extends BaseRepository
{
    public function save($nome, $categoriaId)
    {
        $id = $this->verify($nome, $categoriaId);
        if (!$id) {
            return $this->insert($nome, $categoriaId);
        }
        return $this->update($id, $nome, $categoriaId);
    }

    public function verify($nome, $categoriaId)
    {
        $sql = "SELECT id FROM produto where nome =:nome and categoria_id = :categoriaId";
        $stmt = $this->pdo->prepare($sql);
        $stmt->bindParam(":nome",$nome);
        $stmt->bindParam(":categoriaId",$categoriaId);
        if (!$stmt->execute()) {
            return false;
        }
        $reg = $stmt->fetchObject();
        return $reg->id ?? false;
    }

    public function insert($nome, $categoriaId)
    {
        $sql = "INSERT INTO produto SET nome = :nome, categoria_id = :categoriaId";
        $stmt = $this->pdo->prepare($sql);
        $stmt->bindParam(":nome",$nome);
        $stmt->bindParam(":categoriaId",$categoriaId);
        return $stmt->execute() ? $this->pdo->lastInsertId() : false;
    }

    public function update($id, $nome, $categoriaId)
    {
        $sql = "UPDATE produto SET nome = :nome, categoria_id = :categoriaId WHERE id = :id";
        $stmt = $this->pdo->prepare($sql);
        $stmt->bindParam(":nome",$nome);
        $stmt->bindParam(":categoriaId",$categoriaId);
        $stmt->bindParam(":id",$id);
        return $stmt->execute() ? $id : false;
    }
}