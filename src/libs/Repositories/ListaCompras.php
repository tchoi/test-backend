<?php

namespace App\Repositories;


class ListaCompras extends BaseRepository
{
    public function save($mesId, $produtoId, $qtde)
    {
        $id = $this->verify($mesId, $produtoId, $qtde);
        if (!$id) {
            return $this->insert($mesId, $produtoId, $qtde);
        }
        return $this->update($id, $mesId, $produtoId, $qtde);
    }

    public function verify($mesId, $produtoId, $qtde)
    {
        $sql = "SELECT id FROM lista_compras where mes_id =:mesId AND produto_id = :produtoId AND quantidade = :qtde";
        $stmt = $this->pdo->prepare($sql);
        $stmt->bindParam(":mesId", $mesId);
        $stmt->bindParam(":produtoId", $produtoId);
        $stmt->bindParam(":qtde", $qtde);
        if (!$stmt->execute()) {
            return false;
        }
        $reg = $stmt->fetchObject();
        return $reg->id ?? false;
    }

    public  function insert($mesId, $produtoId, $qtde)
    {
        $sql = "
            INSERT INTO lista_compras SET 
                mes_id = :mesId,
                produto_id = :produtoId,
                quantidade = :qtde
        ";
        $stmt = $this->pdo->prepare($sql);
        $stmt->bindParam(":mesId", $mesId);
        $stmt->bindParam(":produtoId", $produtoId);
        $stmt->bindParam(":qtde", $qtde);
        return $stmt->execute() ? $this->pdo->lastInsertId() : false;
    }

    public function update($id, $mesId, $produtoId, $qtde)
    {
        $sql = "
            UPDATE lista_compras SET 
                mes_id = :mesId,
                produto_id = :produtoId,
                quantidade = :qtde
            WHERE id = :id
        ";
        $stmt = $this->pdo->prepare($sql);
        $stmt->bindParam(":mesId", $mesId);
        $stmt->bindParam(":produtoId", $produtoId);
        $stmt->bindParam(":qtde", $qtde);
        $stmt->bindParam(":id", $id);

        return $stmt->execute() ? $id : false;
    }
}