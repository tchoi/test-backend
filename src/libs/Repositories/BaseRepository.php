<?php

namespace App\Repositories;

use \PDO;
class BaseRepository
{
    /**
     * @var \PDO
     */
    protected $pdo;

    public function __construct()
    {
        $this->pdo = new PDO('mysql:host=mysql.test-backend.test;dbname=testbackend', "testbackend", "destino2002");
        $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }
}
