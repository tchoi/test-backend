<?php

namespace App\Helpers;

final class Csv
{
    private $filename;
    private $separator = "";
    private $header = [];
    private $isHeaderSave = false;

    /**
     * Csv constructor.
     * @param $filename
     * @param string $separator
     */
    public function __construct($filename, string $separator)
    {
        $this->filename = $filename;
        $this->separator = $separator;
    }

    /**
     * @param mixed $header
     */
    public function setHeader($header): void
    {
        $this->header = $header;
    }

    public function createRow($col): string
    {
        return '"' . implode('"' . $this->separator . '"', $col) . '"' . "\n";
    }

    protected function saveHeader(): void
    {
        if (!$this->isHeaderSave) {
            file_put_contents($this->filename, $this->createRow($this->header));
            $this->isHeaderSave = true;
        }
    }

    public function saveRow($col): void
    {
        $this->saveHeader();
        file_put_contents($this->filename, $this->createRow($col), FILE_APPEND);
    }
}
