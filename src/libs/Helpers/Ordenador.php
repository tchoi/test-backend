<?php

namespace App\Helpers;

final class Ordenador
{
    public const DEPARA = [
        "Papel Hignico" => "Papel Higiênico",
        "Brocolis" => "Brócolis",
        "Chocolate ao leit" => "Chocolate ao leite",
        "Sabao em po" => "Sabão em pó",
        "Marco" => "Março",
        "Higiene_pessoal" => "Higiene Pessoal"
    ];
    public const MESES_POR_EXTENSO = [
        'Janeiro' => 1,
        'Fevereiro' => 2,
        'Marco' => 3,
        'Março' => 3,
        'Abril' => 4,
        'Maio' => 5,
        'Junho' => 6,
        'Julho' => 7,
        'Agosto' => 8,
        'Setembro' => 9,
        'Outubro' => 10,
        'Novembro' => 11,
        'Dezembro' => 12
    ];

    private $listaDeCompras = [];

    /**
     * Ordenador constructor.
     * @param array $listaDeCompras
     */
    public function __construct(array $listaDeCompras)
    {
        $this->listaDeCompras = $listaDeCompras;
    }

    public static function valorPorNomeMes($mes)
    {
        return self::MESES_POR_EXTENSO[$mes];
    }

    public static  function dePara($valor)
    {
        return self::DEPARA[$valor] ?? $valor;
    }

    public function ordenar()
    {
        $aListaDeCompras = $this->listaDeCompras;
        $aMeses = array_keys($aListaDeCompras);
        usort($aMeses, function ($mes01, $mes02) {
            return Ordenador::valorPorNomeMes(ucfirst($mes01)) > Ordenador::valorPorNomeMes(ucfirst($mes02));
        });

        $aListaDeComprasCorrigida = [];
        foreach ($aMeses as $mes) {
            if (isset($aListaDeCompras[$mes]) && count($aListaDeCompras[$mes]) > 0) {
                $itens = $aListaDeCompras[$mes];
                ksort($itens);
                $aCategorias = array_keys($itens);
                foreach ($aCategorias as $categoria) {
                    if (count($itens[$categoria]) > 0 ) {
                        $produto = $itens[$categoria];
                        arsort($produto);
                        $itens[$categoria] = $produto;
                    } else {
                        unset($itens[$categoria]);
                    }
                }
                if (count($itens)) {
                    $aListaDeComprasCorrigida[$mes] = $itens;
                }
            }
        }

        $aListaDeCompras = [];
        foreach ($aListaDeComprasCorrigida as $mes => $itens) {
            foreach ($itens as $categoria => $item) {
                foreach ($item as $produto => $qtde) {
                    $aListaDeCompras[] = [
                        "mes" => Ordenador::dePara(ucfirst($mes)),
                        "categoria" => Ordenador::dePara(ucfirst($categoria)),
                        "produto" => Ordenador::dePara($produto),
                        "qtde" => $qtde
                    ];
                }
            }
        }

        return $aListaDeCompras;
    }
}
