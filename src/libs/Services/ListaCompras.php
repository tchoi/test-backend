<?php

namespace App\Services;


use App\Helpers\Ordenador;
use App\Repositories\Categoria;
use App\Repositories\ListaCompras as ListaComprasRepository;
use App\Repositories\Mes;
use App\Repositories\Produto;

class ListaCompras
{
    protected $categoriaRepository;
    protected $produtoRepository;
    protected $listaComprasRepository;

    /**
     * ListaCompras constructor.
     */
    public function __construct()
    {
        $this->categoriaRepository = new Categoria();
        $this->produtoRepository = new Produto();
        $this->listaComprasRepository = new ListaComprasRepository();
    }

    public function save($itemLista)
    {
        $mesId = Ordenador::MESES_POR_EXTENSO[$itemLista['mes']];
        $categoriaId = $this->categoriaRepository->save($itemLista['categoria']);
        $produtoId = $this->produtoRepository->save($itemLista['produto'], $categoriaId);
        $listaId = $this->listaComprasRepository->save($mesId, $produtoId, $itemLista['qtde']);
        return $listaId;
    }
}