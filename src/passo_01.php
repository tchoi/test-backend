<?php
include_once __DIR__ . "/vendor/autoload.php";

use App\Helpers\Csv;
use App\Helpers\Ordenador;

$aListaDeCompras = include_once __DIR__ . '/lista-de-compras.php';
$ordenador = new Ordenador($aListaDeCompras);
$listaOrdenada = $ordenador->ordenar();
$aCabecalho = [
    "Mês" => "mes",
    "Categoria" => "categoria",
    "Produto" => "produto",
    "Quantidade" => "qtde"
];

$csv = new Csv(__DIR__ . "/compras-do-ano.csv", ",");
$csv->setHeader(array_keys($aCabecalho));
foreach ($listaOrdenada as $item) {
    $aLinha = [];
    foreach ($aCabecalho as $valor) {
        $aLinha[] = $item[$valor];
    }
    $csv->saveRow($aLinha);
}