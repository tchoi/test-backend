<?php
function dePara($de)
{
    $aDePara = [
        "Papel Hignico" => "Papel Higiênico",
        "Brocolis" => "Brócolis",
        "Chocolate ao leit" => "Chocolate ao leite",
        "Sabao em po" => "Sabão em pó",
        "Marco" => "Março",
        "Higiene_pessoal" => "Higiene Pessoal"
    ];
    return $aDePara[$de] ?? $de;
}

function todosMeses()
{
    $aMes = [
        'Janeiro' => 1,
        'Fevereiro' => 2,
        'Marco' => 3,
        'Março' => 3,
        'Abril' => 4,
        'Maio' => 5,
        'Junho' => 6,
        'Julho' => 7,
        'Agosto' => 8,
        'Setembro' => 9,
        'Outubro' => 10,
        'Novembro' => 11,
        'Dezembro' => 12
    ];
    return $aMes;
}

function valorPorNomeMes($mes)
{
    $aMes = todosMeses();
    return $aMes[$mes];
}


$aListaDeCompras = include_once __DIR__ . '/lista-de-compras.php';
$aMeses = array_keys($aListaDeCompras);
usort($aMeses, function($mes01, $mes02) {
    return valorPorNomeMes(ucfirst($mes01)) > valorPorNomeMes(ucfirst($mes02));
});
$aListaDeComprasCorrigida = [];
foreach ($aMeses as $mes) {
    if (isset($aListaDeCompras[$mes]) && count($aListaDeCompras[$mes]) > 0) {
        $itens = $aListaDeCompras[$mes];
        ksort($itens);
        $aCategorias = array_keys($itens);
        foreach ($aCategorias as $categoria) {
            if (count($itens[$categoria]) > 0 ) {
                $produto = $itens[$categoria];
                arsort($produto);
                $itens[$categoria] = $produto;
            } else {
                unset($itens[$categoria]);
            }
        }
        if (count($itens)) {
            $aListaDeComprasCorrigida[$mes] = $itens;
        }
    }
}
$aListaDeCompras = [];
foreach ($aListaDeComprasCorrigida as $mes => $itens) {
    foreach ($itens as $categoria => $item) {
        foreach ($item as $produto => $qtde) {
            $aListaDeCompras[] = [
                "mes" => dePara(ucfirst($mes)),
                "categoria" => dePara(ucfirst($categoria)),
                "produto" => dePara($produto),
                "qtde" => $qtde
            ];
        }
    }
}

return $aListaDeCompras;