#!/bin/bash
set -e
echo "[ ****************** ] Starting Endpoint of Application"
/usr/bin/supervisord
/usr/bin/crontab
echo "[ ****************** ] Ending Endpoint of Application"
exec "$@"
